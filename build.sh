rm atlasLoad.tar.gz
rm -rf pkg_osx pkg_linux
mkdir pkg_linux
mkdir pkg_osx
tar -xzf gor*PRO_mac.tar.gz
mv gor pkg_osx/atlasload
tar -xzf gor*PRO_x64.tar.gz
mv gor pkg_linux/atlasload
pkg -t node6-linux-x64 ./middleware_record.js -o pkg_linux/middleware_record
pkg -t node6-linux-x64 ./middleware_replay.js -o pkg_linux/middleware_replay
pkg -t node6-osx-x64 ./middleware_replay.js -o pkg_osx/middleware_replay
pkg -t node6-osx-x64 ./middleware_record.js -o pkg_osx/middleware_record
cp start*.sh pkg_linux/
cp start*.sh pkg_osx/
cp stop*.sh pkg_linux/
cp stop*.sh pkg_osx/
chmod +x pkg_linux/*
chmod +x pkg_osx/*
touch pkg_linux/results.csv pkg_osx/results.csv
python3 -m markdown -x markdown.extensions.fenced_code -x  markdown.extensions.codehilite -x markdown.extensions.smarty -x markdown.extensions.toc README.md > README.html
cp README.* pkg_linux/
cp README.* pkg_osx/
tar czvf atlasLoad.tar.gz pkg_osx pkg_linux authenticator-*.jar
