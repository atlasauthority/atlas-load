#!/bin/bash

declare -a pidarray
let i=0
while IFS=$'\n' read -r line_data; do
    pidarray[i]="${line_data}"
    ((++i))
done < atlasload_replay.pid

echo "Stopping Atlas Load replay"

for ((c=0; c < i; c++)) do
    PID=${pidarray[$c]}
    while kill -0 "$PID" >/dev/null 2>&1
    do
        kill "$PID"
        sleep 0.2
    done
done

# We're seeing some of these left as zombie processes 
killall middleware_replay
cat /dev/null > atlasload_replay.pid