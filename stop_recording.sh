#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root, or with sudo" 
   exit 1
fi
PID=$(< atlasload_record.pid)
echo "Stopping Atlas Load recording"
while kill -0 "$PID" >/dev/null 2>&1
do
    kill "$PID"
    sleep 0.2
done

# We're seeing some of these left as zombie processes 
killall middleware_record
cat /dev/null > atlasload_record.pid