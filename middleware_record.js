#!/usr/bin/env node
var gor = require('goreplay_middleware')
gor.init()
const password = 'sphere'

// Handle removing passwords from the recording
gor.on('request', function (req) {
  // console.error(req.http)
  // console.error(gor.httpPath(req.http))
  let path = gor.httpPath(req.http)
  // console.error(1)
  if (path.indexOf('/rest/gadget/1.0/login') !== -1 || path.indexOf('/login.jsp') !== -1) {
    // console.error(2)
    let username = gor.httpBody(req.http).toString('utf-8').match('os_username=(.*)&os_password')
    // console.error(3)
    if (username) {
      // console.error(4)
      username = username[1]
      // console.error(5)
      req.http = gor.setHttpBody(req.http, Buffer.from(`os_username=${username}&os_password=${password}`))
      // console.error(6)
    }
    // console.error(7)
  }

  // Login via REST API
  if (path.indexOf('/rest/auth/1/session') !== -1) {
    // console.error(8)
    let username = gor.httpBody(req.http).toString('utf-8').match('username": "([^"]+)"')
    // console.error(9)
    if (username) {
      // console.error(10)
      username = username[1]
      // console.error(11)
      req.http = gor.setHttpBody(req.http, Buffer.from(`{"username": "${username}", "password": "${password}"}`))
      /// /console.error(gor.httpBody(req.http).toString('utf-8'))
      // console.error(12)
    }
  }

  return req
})
