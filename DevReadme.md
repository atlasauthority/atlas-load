Dev Readme

You need to get GoReplay from https://github.com/buger/goreplay/releases

"Build" procedure

./build.sh is all you need to do.

To generate load, start up JIRA, then login and replace the body with something like:

```<body><iframe id="testFrame" src="http://localhost:8080/secure/RapidBoard.jspa?rapidView=1&useStoredSettings=true" style="position: absolute; top:0; left:0; right:0; bottom:0; width:100%; height:100%;" onload="setInterval(document.getElementById('testFrame').contentWindow.location.reload(), 1000000);"></iframe></body>```

I did 3 of these, 1 on dashboard, 1 on issueload, and 1 on agile board

Use normal instructions for actually using the tool
