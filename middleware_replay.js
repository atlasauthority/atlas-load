#!/usr/bin/env node

const help = `
AtlasLoad Middleware for replaying Jira API sessions with cookie based authentification.
See https://github.com/buger/goreplay/wiki/Middleware for documentation on how middleware works. 
Basic idea is that middleware receive Hex encoded messages at STDIN and emits them back to STDOUT.
Use STDERR (console.error) for middleware debugging output.
Example of providing it as argument to AtlasLoad: \`atlasload ... --middleware="./middleware.js --user fakeuser --pass 123456789"\`
You can specify username \`--user\` and password: \`--pass\`.
Middleware support both REST API and regular UI auth types. Should be detected automatically.
To run unit tests use: \`./middleware.js test\`.
`

var gor = require('goreplay_middleware')
gor.init()

var fs = require('fs')

var config = {
  user: null,
  pass: 'sphere',
  single_user: false,
  product: 'jira',
  crowdSessionCookie: 'crowd.token_key',
  auth: 'header',
  context: ''
}

// Read command line arguments
process.argv.forEach(function (val, index) {
  switch (val) {
    case 'test':
      if (index === 2) {
        gor.test()
        process.exit()
      }
      break
    case '--user':
      config.user = process.argv[index + 1]
      break
    case '--pass':
      config.pass = process.argv[index + 1]
      break
    case '--single-user':
      config.single_user = true
      console.error('Running middleware in single user mode')
      break
    case '--product':
      config.product = process.argv[index + 1]
      break
    case '--auth':
      config.auth = process.argv[index + 1]
      break
    case '--context':
      config.context = process.argv[index + 1]
      break
    case '--help':
      console.error(help)
      process.exit()
  }
})

var latestUserName = null

// Will store mappings between original and replayed session ids
var sessionMap = {}
var latestSessionID = null
var crowdSessionMap = {}
var latestCrowdSessionID = null
var attachIdMap = {}

// Used to extract value from replayed response
function searchReplay (id, searchPattern, callback) {
  let re = new RegExp(searchPattern)

  // Using regexp require converting buffer to string
  // Before converting to string we can use initial `Buffer.indexOf` check
  let indexPattern = searchPattern.split('(')[0]

  if (!indexPattern) {
    console.error('Search regexp should include capture group, pointing to the value: `prefix-(.*)`')
    return
  }

  gor.on('replay', id, function (repl) {
    if (repl.http.indexOf(indexPattern) === -1) {
      callback()
      return repl
    }

    let replMatch = repl.http.toString('utf-8').match(re)

    if (!replMatch) {
      callback()
      return repl
    }

    callback(replMatch[1], repl)

    return repl
  })
}

function replaceRequest (req, searchPattern, replaceCallback) {
  let re = new RegExp(searchPattern, 'm')

  let reqString = req.http.toString('utf-8')
  let reqMatch = reqString.match(re)
  if (!reqMatch) {
    return
  }

  let value = reqMatch[1]
  let replacement = replaceCallback(value) || ''
  let matchValue = reqMatch[0]
  let matchReplacement = matchValue.replace(value, replacement)
  let reqReplaced = reqString.replace(matchValue, matchReplacement)

  req.http = Buffer.from(reqReplaced)
}

// Handle JIRA session tokens and auth
gor.on('request', function (req) {
  gor.on('response', req.ID, function (resp) {
    let nameHeader = gor.httpHeader(resp.http, 'X-AUSERNAME')
    if (nameHeader) {
      let nameHeaderValue = nameHeader['value']
      // console.error("set X-AUSERNAME", `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, nameHeaderValue);
      if (nameHeaderValue !== 'anonymous') {
        latestUserName = nameHeaderValue
      }
    }

    return resp
  })

  // Replace cookie tokens in all following requests
  let sessionID = gor.httpCookie(req.http, 'JSESSIONID')
  let replSessionID = config.single_user ? latestSessionID : sessionMap[sessionID]

  if (sessionID) {
    req.http = gor.setHttpCookie(req.http, 'JSESSIONID', replSessionID || '')
  }
  // console.error(`${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, "JSESSIONID", sessionID, replSessionID)

  // handle recorded login requests which set session id
  gor.searchResponses(req.ID, 'Set-Cookie: JSESSIONID=([^;]+)', function (respSession, replSession) {
    if (respSession && replSession) {
      // console.error("set JSESSIONID", `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, respSession, replSession)
      sessionMap[respSession] = replSession
      latestSessionID = replSession
    }
  })

  // handle all other requests which can set session id because of basic auth
  searchReplay(req.ID, 'Set-Cookie: JSESSIONID=([^;]+)', function (replSession, repl) {
    if (!replSession) {
      return
    }
    // ignore anonymous user sessions
    let nameHeader = gor.httpHeader(repl.http, 'X-AUSERNAME')
    if (!nameHeader || nameHeader['value'] === 'anonymous') {
      // TODO: Figure out a way to handle requests where this is empty because the impact is that we fail to set a JSESSIONID and then the request fails to auth. I believe line 167 may be what is doing the real damage.
      return
    }
    // console.error("set JSESSIONID2", `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, sessionID, replSession)
    latestSessionID = replSession
    if (sessionID && !sessionMap.hasOwnProperty(sessionID)) {
      sessionMap[sessionID] = replSession
    }
  })

  // Replace crowd cookie tokens in all following requests
  let crowdSessionID = gor.httpCookie(req.http, config.crowdSessionCookie)
  let replCrowdSessionID = config.single_user ? latestCrowdSessionID : crowdSessionMap[crowdSessionID]

  if (crowdSessionID) {
    req.http = gor.setHttpCookie(req.http, config.crowdSessionCookie, replCrowdSessionID || '')
  }
  // console.error(`${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, "cookie", config.crowdSessionCookie, crowdSessionID, replCrowdSessionID)

  // handle recorded requests which set crowd session id (omit searchReplay() because id updated by all requests)
  gor.searchResponses(req.ID, 'Set-Cookie: ' + config.crowdSessionCookie + '=([^;]{3,})', function (respSession, replSession) {
    if (respSession && replSession) {
      // console.error("set cookie", config.crowdSessionCookie, `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, respSession, replSession)
      crowdSessionMap[respSession] = replSession
      latestCrowdSessionID = replSession
    }
  })

  let path = gor.httpPath(req.http)

  // Login via UI
  if (path.indexOf('/rest/gadget/1.0/login') !== -1 || (path.indexOf('/login.jsp') !== -1 && gor.httpMethod(req.http) === 'POST')) {
    // reset session id for login requests because jira does not set new session id for logged-in (with basic auth) user
    // and sessionMap entry is not filled
    if (replSessionID) {
      req.http = gor.setHttpCookie(req.http, 'JSESSIONID', '')
    }
    if (config.user) {
      req.http = gor.setHttpBody(req.http, Buffer.from(`os_username=${config.user}&os_password=${config.pass}`))
    }
  } else if (path.indexOf('/rest/auth/1/session') !== -1) { // Login via REST API
    if (config.user) {
      req.http = gor.setHttpBody(req.http, Buffer.from(`{"username": "${config.user}", "password": "${config.pass}"}`))
    }

    // Extracting value from JSON auth response
    gor.searchResponses(req.ID, '"value": "([^"]+)"', function (respSession, replSession) {
      if (respSession && replSession) {
        sessionMap[respSession] = replSession
        latestSessionID = replSession
      }
    })
  } else if (sessionID && !replSessionID) { // Basic auth
    let user = config.user ? config.user : latestUserName
    if (user) {
      // console.error("auth", config.auth, `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, user)
      switch (config.auth) {
        case 'basic':
          let auth = 'Basic ' + Buffer.from(`${user}:${config.pass}`).toString('base64')
          req.http = gor.setHttpHeader(req.http, 'Authorization', auth)
          break
        case 'header':
          req.http = gor.setHttpHeader(req.http, 'X-ALOADUSERNAME', user)
          break
      }
    }
  }

  if (path.indexOf('AttachTemporaryFile') !== -1) {
    gor.searchResponses(req.ID, '"id":"([^"]+)"', function (respAttachId, replAttachId) {
      if (respAttachId && replAttachId) {
        // console.error("set attach id", `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, respAttachId, replAttachId)
        attachIdMap[respAttachId] = replAttachId
      }
    })
  }
  if (path.indexOf('BulkCreateSetupPage') !== -1 && gor.httpMethod(req.http) === 'POST') {
    replaceRequest(req, 'Content-Disposition: form-data; name="filetoconvert"\\s+([^\r\n]+)', function (attachId) {
      // console.error("replace attach id", `${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, attachId, attachIdMap[attachId])
      return attachIdMap[attachId]
    })
  }

  // Confluence writing pages. We fix the timestamp in the path to be now.
  if (config.product === 'confluence') {
    if (gor.httpPathParam(req.http, '_')) {
      req.http = gor.setHttpPathParam(req.http, '_', (new Date()).getTime())
    }
  }

  return req
})

// Replace XSRF tokens
gor.on('request', function (req) {
  let hasToken = req.http.indexOf('atlassian.xsrf.token') !== -1 || req.http.indexOf('atl_token') !== -1 || req.http.indexOf('formToken=') !== -1
  if (hasToken) {
    req.http = gor.setHttpHeader(req.http, 'X-Atlassian-Token', 'no-check')
    // console.error(`${gor.httpMethod(req.http)} ${gor.httpPath(req.http)}`, "xsrf.token (no check)")
  }

  return req
})

// Remove Session Affinity for Apache
// gor.on("request", function(req) {
//    req.http = gor.deleteHttpHeader(req.http, "Set-Cookie: ROUTEIDBLABLA");
//    return req;
// })

// Removing Headers
/*
gor.on('request', function (req) {
  const badHeaders = [
    'X-Forwarded-For',
    'X-Forwarded-Host',
    'X-Forwarded-Server',
    'X-Amzn-Trace-Id',
    'X-Amz-Cf-Id',
    'Via',
    'CloudFront-Is-Mobile-Viewer',
    'CloudFront-Is-Tablet-Viewer',
    'CloudFront-Is-SmartTV-Viewer',
    'CloudFront-Is-Desktop-Viewer',
    'CloudFront-Viewer-Country',
    'CloudFront-Forwarded-Proto',
    'CDN',
    'X-ANODEID',
    'X-Seraph-LoginReason',
    'X-ASESSIONID',
    'X-AREQUESTID'
  ]

  for (var i = 0; i < badHeaders.length; i++) {
    //req.http = gor.deleteHttpHeader(req.http, badHeaders[i]);
  }

  return req
})
*/

// Whats the best way to figure out the averages for responses? Need to figure out generalized URL paths most likely?

var stream = fs.createWriteStream('results.csv', { flags: 'a' })

gor.on('request', function (req) {
  const badPaths = [
    '/rest/webResources/1.0/resources',
    '/rest/analytics/1.0/publish/bulk',
    '/plugins/servlet/gadgets/dashboard-diagnostics'
  ]

  for (var i = 0; i < badPaths.length; i++) {
    badPaths[i] = config.context + badPaths[i]
  }

  if (badPaths.includes(gor.httpPath(req.http))) {
    return // eslint-disable-line no-useless-return
  } else {
    gor.on('response', req.ID, function (resp) {
      gor.on('replay', req.ID, function (repl) {
        // console.error(gor.httpPath(req.http))
        // console.error(repl.meta[3]/ 1000000.0 + " ms")

        let referer
        if (gor.httpHeader(req.http, 'Referer')) {
          referer = gor.httpHeader(req.http, 'Referer')['value']
        }
        let xausername
        if (gor.httpHeader(resp.http, 'X-AUSERNAME')) {
          xausername = gor.httpHeader(resp.http, 'X-AUSERNAME')['value']
        }
        // console.error(req.meta)
        // console.error(resp.meta)
        stream.write(`"${gor.httpPath(req.http)}","${gor.httpMethod(req.http)}","${referer}",${resp.meta[4] / 1000000.0},${repl.meta[3] / 1000000.0},${xausername},${gor.httpStatus(resp.http)},${gor.httpStatus(repl.http)}\n`)

        if (gor.httpStatus(resp.http) !== gor.httpStatus(repl.http)) {
          console.error(`${gor.httpMethod(req.http)} ${gor.httpPath(req.http)} HTTP response code did not match. Expected: '${gor.httpStatus(resp.http)}' got '${gor.httpStatus(repl.http)}'`)
        }
        return repl
      })

      return resp
    })

    return req
  }
})
