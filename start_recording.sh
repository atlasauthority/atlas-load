#!/bin/bash

#SET OPTIONS HERE
####################################################################################
OUTPUT=./
INPUT=:8080
FOREGROUND=false

#REMEMBER THAT THIS PROCESS RUNS WITH SUDO
#You need to set these environment variables either in this file, or with sudo
if [ -z ${AWS_ACCESS_KEY+x} ]; then 
    export AWS_ACCESS_KEY=
fi
if [ -z ${AWS_SECRET_KEY+x} ]; then 
    export AWS_SECRET_KEY=
fi   
if [ -z ${AWS_REGION+x} ]; then 
    export AWS_REGION=
fi
####################################################################################


#DON'T SET THESE
####################################################################################
DEBUG=
VERBOSE=
EXIT=false
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
APPEND=--output-file-append
MIDDLEWARE="./middleware_record"
####################################################################################

helpmenu(){
    echo "Usage: $0 [options...]"
    echo
    echo "  -i, --input            Traffic to record. Eg: localhost:8080"
    echo "  -o, --output           Destination, should be a folder. Eg: /tmp"
    echo "  -f, --foreground       Run with logs in the foreground"
    echo "  -h, --help             Print this menu"
    echo
    exit 1
}

while [ ! $# -eq 0 ]
do
	case "$1" in
		--help | -h)
			helpmenu
			exit
			;;
		--output | -o | --output-file)
			OUTPUT=$2
			;;
		--input | -i | --input-raw)
			INPUT=$2
			;;
        --foreground | -f)
			FOREGROUND=true
			;;
        --verbose | -v)
			VERBOSE=--verbose
			;;
        --debug | -d)
			DEBUG=--debug
			;;
        --develop)
            pkill -f 'node middleware_record.js' 
            CONTEXT=
			MIDDLEWARE="node middleware_record.js"
			;;
	esac
	shift
done

# S3 handling
if [[ $OUTPUT = *"s3"* ]]; then
    echo "Using S3 for output"

    if [ -z ${AWS_ACCESS_KEY+x} ]; then 
        echo "Please set the AWS_ACCESS_KEY environment variable";
        EXIT=true 
    fi
    if [ -z ${AWS_SECRET_KEY+x} ]; then 
        echo "Please set the AWS_SECRET_KEY environment variable";
        EXIT=true
    fi   
    if [ -z ${AWS_REGION+x} ]; then 
        echo "Please set the AWS_REGION environment variable";
        EXIT=true
    fi
    if [ "$EXIT"=false ] ; then
        APPEND="";
    fi
fi

# Check for sudo
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root, or with sudo" 
   EXIT=true
fi

if [ "$EXIT" = true ] ; then
    exit 1
fi

echo "Starting Atlas Load recording"

if [ "$DEBUG" = --debug ] ; then
    set -x;
fi
if [ "$FOREGROUND" = true ] ; then
    echo "Ctrl-c to quit"
    echo
    "$DIR"/atlasload $DEBUG $VERBOSE --input-raw "$INPUT" --recognize-tcp-sessions --input-raw-track-response --middleware="$MIDDLEWARE" --output-file $OUTPUT/atlasload-capture-`hostname`-%Y-%m-%d-%H.log --prettify-http $APPEND --output-file-size-limit 2m
else
    echo "Run stop_record.sh to end the recording" 
    nohup "$DIR"/atlasload $DEBUG $VERBOSE --input-raw "$INPUT" --recognize-tcp-sessions --input-raw-track-response --middleware="$MIDDLEWARE" --output-file $OUTPUT/atlasload-capture-`hostname`-%Y-%m-%d-%H.log --prettify-http $APPEND --output-file-size-limit 2m 0<&- &> $DIR/atlasload_record.log &
    echo $! > atlasload_record.pid
fi
if [ "$DEBUG" = --debug ] ; then
    set +x;
fi