# Errors #
This document is a bit outdated, you shoud review the start and stop scripts to better understand the common usage logic. You will also need to look at https://bitbucket.org/atlasauthority/atlas-load-authenticator/src for the authenticator portion

[TOC]

# Atlas Load #

Capture and replay production traffic from Atlassian applications
___
## Requirements ##
* Linux or OSX
* Root access on the recording system
* Storage space equal to the total network traffic in and out of your application during the recorded period
___
## Usage: ##

This section assumes that you are running Atlas Load on the same server where your Atlassian application record or replay target is running.

1. Open a terminal and navigate into _pkg_linux_ or _pkg_osx_ directory depending on your OS
1. Run _start_recording.sh_ file and set 
    * The -i flag to your input source. Eg: -i localhost:8080
    * The -o flag to your output destination. Eg: -o /tmp
1. To stop recording, run _stop_recording.sh_
1. Update all user passwords in Jira to be "_sphere_":
```sql
update cwd_user set credential='uQieO/1CGMUIXXftw3ynrsaYLShI+GTcPS4LdUGWbIusFvHPfUzD7CZvms6yMMvA8I7FViHVEqr6Mj4pCLKAFQ==';
```
1. Update the Base Url in the Target instance to match the Source instance.
1. To begin the replay, run _start_replay.sh_
    * The -i flag to your input source. E.g. -i /tmp/atlasload-capture-*
    * The -o flag to your output destination. E.g. -o localhost:8080
1. To stop recording, run _stop_replay.sh_
1. Review the output in results.csv

### What we don't reproduce ###
* Anything that isn't over HTTP
    * Mail Handlers
    * Custom integrations using other protocols
* Application Links
    * This may actually work, but due to the number of possible permutations we have not tested this.

___
## Advanced usage: ##

Atlas Load is an expansion on top of the Pro version of the open source tool [GoReplay](https://goreplay.org/). Now that we're open sourcing it, you should try using the free version. We're not making any waranties or anything here. 

### Advanced Script Options ###
Some of these options have been added to the startup scripts for ease of use.\
In start_replay.sh

* -p, --parallel         
    * Number of replays to run in parallel.
* -f, --foreground       
    * Run with logs in the foreground
* -l, --looping          
    * Loop replay file until manually killed
* -s, --speedup          
    * Percentage speed you want the replay to happen at. Default is 100. E.g. 150 would mean 150%. Do not include the % sign.

Parallel and Looping **can** be combined.  
Looping and Foreground **can** be combined.  
Parallel and Foreground **can not be** combined.  
Speedup will work in all scenarios.   

You can build upon the bundled scripts to extend the functionality.

### Application State ###

Since many operations in Atlassian applications are stateful, the closer the destination environment is to the source when the recording occurred, the better. Typically this means taking a DB snapshot at the same time as you start your recording. You can also take a snapshot of your Lucene index on the filesystem at the same time.

### Where do you run Atlas Load ###

Depending on your network infrastructure, you can run Atlas Load on a machine other than your Atlassian application server. See [these docs](https://github.com/buger/goreplay/wiki/Distributed-configuration).

We do **NOT** recommend that you use Atlas Load as a proxy for your application. This configuration adds no value and introduces a significant amount of risk.

You must run this at a point in your network where the traffic is unencrypted.

### Where to store the captured data ###
You can store these anywhere on your filesystem. We strongly encourage you to save recordings on a volume that does not share IO with your Atlassian application. 

For applications that run in AWS, we have native support for direct access to S3 buckets. See [these docs](https://github.com/buger/goreplay/wiki/%5BPRO%5D-Using-S3-for-storing-and-replaying-traffic). You will need to customize the start_recording.sh script to put it's log files outside the output location. Look at lines 49 and 52. However, we strongly recommend that you use an EBS or EFS volume instead of S3. S3 does not allow us to append data to a file, and this means we will have to create a huge number of small chunked files instead of clean ones that are chunked by the hour. 
___
## Security ##

Because we are capturing raw HTTP traffic, we see everything in the clear, including user passwords. To comply with legislation, and security best practices, we are rewriting all passwords to be _sphere_ in the recorded file. 

We no longer require that you have control over user passwords to replay a recording. You do need to install our Atlas Load Authenticator app for Jira (included in the zip file), and then configure it to whitelist the IP address from which you will be replaying the traffic. The IP address should be in [CIDR notation](https://www.ietf.org/rfc/rfc1878.txt).

There can be a lot of other sensitive data in there beyond passwords. If you require more advanced filtering, please reach out for help.
___
## Supported platforms ##
* Record Jira (Core, Software, and Service Desk). 
* Replay Jira any version
* Record Confluence any version
* Replay Confluence read operations
	* Support for write operations is in progress
___
## Known Issues ##
* None
