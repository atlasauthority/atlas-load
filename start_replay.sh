#!/bin/bash

#SET OPTIONS HERE
####################################################################################
OUTPUT=localhost:8080
INPUT=./atlasload-capture-*
FOREGROUND=false
PARALLEL=1
LOOPING=""
#100 is normal speed, 200 is 2x, etc
SPEEDUP=100
#Remember to include a leading /
CONTEXT=""

if [ -z ${AWS_ACCESS_KEY+x} ]; then 
    export AWS_ACCESS_KEY=
fi
if [ -z ${AWS_SECRET_KEY+x} ]; then 
    export AWS_SECRET_KEY=
fi   
if [ -z ${AWS_REGION+x} ]; then 
    export AWS_REGION=
fi
####################################################################################

#DON'T SET THESE
####################################################################################
DEBUG=
VERBOSE=
EXIT=false
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MIDDLEWARE="./middleware_replay --auth header --context $CONTEXT"
####################################################################################

helpmenu(){
    echo "Usage: $0 [options...]"
    echo
    echo "  -i, --input            File to replay. Wildcards are accepted."
    echo "  -o, --output           Destination. E.g. localhost:8080"
    echo "  -p, --parallel         Number of times to run in parallel."
    echo "  -f, --foreground       Run with logs in the foreground"
    echo "  -l, --looping          Loop replay file until manually killed"
    echo "  -s, --speedup          Percentage speed you want the replay to happen at. E.g. 150"
    echo "  -h, --help             Print this menu"
    echo
    exit 1
}

while [ ! $# -eq 0 ]
do
	case "$1" in
		--help | -h)
			helpmenu
			exit
			;;
		--output | -o | -output-http)
			OUTPUT=$2
			;;
		--speedup | -s)
			SPEEDUP=$2
			;;
		--input | -i | --input-file)
			INPUT=$2
			;;
        --foreground | -f)
			FOREGROUND=true
			;;
		--parallel | -p)
            PARALLEL=$2
            ;;
        --looping | -l)
            LOOPING=--input-file-loop
            ;;
        --verbose | -v)
			VERBOSE=--verbose
			;;
        --debug | -d)
			DEBUG=--debug
			;;
        --develop)
            pkill -f 'node middleware_replay.js' 
            CONTEXT=/jira
			MIDDLEWARE="node middleware_replay.js --auth header --context $CONTEXT"
			;;
	esac
	shift
done

# S3 handling
if [[ $INPUT = *"s3"* ]]; then
    echo "Using S3 for output"

    if [ -z ${AWS_ACCESS_KEY+x} ]; then 
        echo "Please set the AWS_ACCESS_KEY environment variable";
        EXIT=true 
    fi
    if [ -z ${AWS_SECRET_KEY+x} ]; then 
        echo "Please set the AWS_SECRET_KEY environment variable";
        EXIT=true
    fi   
    if [ -z ${AWS_REGION+x} ]; then 
        echo "Please set the AWS_REGION environment variable";
        EXIT=true
    fi
    if [ "$EXIT"=false ] ; then
        APPEND="";
    fi
fi

if [ "$EXIT" = true ] ; then
    exit 1
fi

if [ -s "$DIR"/atlasload_replay.pid ]; then
    if pgrep -x "atlasload" > /dev/null; then
        echo "You must stop all running Atlas Load replays before starting new ones"
        exit 1 
    else
        cat /dev/null > "$DIR"/atlasload_replay.pid
    fi

fi

echo "Starting Atlas Load replay"
touch "$DIR/results.csv"
cat /dev/null > "$DIR/results.csv"
echo 'path,method,referer,original timing in ms, replay timing in ms, username,original response code, replay response code\n' > "$DIR/results.csv"

if [ "$FOREGROUND" = true ] ; then
    if [ "$PARALLEL" -gt 1 ] ; then
	echo "You can not use both Parallel and Foreground options together"
        exit 1
    fi
    echo "Ctrl-c to quit"
	echo
    "$DIR"/atlasload $DEBUG $VERBOSE --input-file ""$INPUT"|"$SPEEDUP"%" --middleware="$MIDDLEWARE" --output-http "$OUTPUT" --output-http-track-response --prettify-http "$LOOPING" 
else
    for ((i =1;  i<=  $PARALLEL; i++)) do
        echo "Starting thread "$i""
        nohup "$DIR"/atlasload $DEBUG $VERBOSE --input-file ""$INPUT"|"$SPEEDUP"%" --middleware="$MIDDLEWARE" --output-http "$OUTPUT" --output-http-track-response --prettify-http "$LOOPING"  0<&- &> "$DIR"/atlasload_replay.log &
        echo $! >> "$DIR"/atlasload_replay.pid
    done
    echo "Run stop_replay.sh to prematurely end the replay"
fi
